#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Dict
{
	int valid;
	char data[50];
	char def[100];
	struct Dict *a,*b,*c,*d,*e,*f,*g,*h,*i,*j,*k,*l,*m,*n,*o,*p,*q,*r,*s,*t,*u,*v,*w,*x,*y,*z;
}Dict;

Dict *newDict()
	{
		Dict *dd=(Dict *)malloc(sizeof(Dict));
		dd->a=NULL;dd->b=NULL;dd->c=NULL;dd->d=NULL;dd->e=NULL;dd->f=NULL;dd->g=NULL;dd->h=NULL;dd->i=NULL;dd->j=NULL;dd->k=NULL;
		dd->l=NULL;dd->m=NULL;dd->n=NULL;dd->o=NULL;dd->p=NULL;dd->q=NULL;dd->r=NULL;dd->s=NULL;dd->t=NULL;dd->u=NULL;dd->v=NULL;
		dd->w=NULL;dd->x=NULL;dd->y=NULL;dd->z=NULL;
		dd->valid=-1;
	return dd;
	}

int wordDict(int status,Dict *d,char *word,char *def)
	{
		//printf("%d",strlen(word));
		for(int i=0;i<strlen(word);i++)
		{
			switch(word[i])
			{
				case 'a':
				case 'A':					
					if(d->a==NULL)
					{
						d->a=newDict();
						d=d->a;
					}
					else
						d=d->a;
					break;

				case 'b':
				case 'B':					
					if(d->b==NULL)
					{
						d->b=newDict();
						d=d->b;
					}
					else
						d=d->b;
					break;
				
				case 'c':
				case 'C':					
					if(d->c==NULL)
					{
						d->c=newDict();
						d=d->c;
					}
					else
						d=d->c;
					break;
				case 'd':
				case 'D':					
					if(d->d==NULL)
					{
						d->d=newDict();
						d=d->d;
					}
					else
						d=d->d;
					break;
				case 'e':
				case 'E':					
					if(d->e==NULL)
					{
						d->e=newDict();
						d=d->e;
					}
					else
						d=d->e;
					break;
				case 'f':
				case 'F':					
					if(d->f==NULL)
					{
						d->f=newDict();
						d=d->f;
					}
					else
						d=d->f;
					break;
				case 'g':
				case 'G':					
					if(d->g==NULL)
					{
						d->g=newDict();
						d=d->g;
					}
					else
						d=d->g;
					break;
				case 'h':
				case 'H':					
					if(d->h==NULL)
					{
						d->h=newDict();
						d=d->h;
					}
					else
						d=d->h;
					break;
				case 'i':
				case 'I':					
					if(d->i==NULL)
					{
						d->i=newDict();
						d=d->i;
					}
					else
						d=d->i;
					break;
				case 'j':
				case 'J':					
					if(d->j==NULL)
					{
						d->j=newDict();
						d=d->j;
					}
					else
						d=d->j;
					break;
				case 'k':
				case 'K':					
					if(d->k==NULL)
					{
						d->k=newDict();
						d=d->k;
					}
					else
						d=d->k;
					break;
				case 'l':
				case 'L':					
					if(d->l==NULL)
					{
						d->l=newDict();
						d=d->l;
					}
					else
						d=d->l;
					break;
				case 'm':
				case 'M':					
					if(d->m==NULL)
					{
						d->m=newDict();
						d=d->m;
					}
					else
						d=d->m;
					break;
				case 'n':
				case 'N':					
					if(d->n==NULL)
					{
						d->n=newDict();
						d=d->n;
					}
					else
						d=d->n;
					break;
				case 'o':
				case 'O':					
					if(d->o==NULL)
					{
						d->o=newDict();
						d=d->o;
					}
					else
						d=d->o;
					break;
				case 'p':
				case 'P':					
					if(d->p==NULL)
					{
						d->p=newDict();
						d=d->p;
					}
					else
						d=d->p;
					break;
				case 'q':
				case 'Q':					
					if(d->q==NULL)
					{
						d->q=newDict();
						d=d->q;
					}
					else
						d=d->q;
					break;
				case 'r':
				case 'R':					
					if(d->r==NULL)
					{
						d->r=newDict();
						d=d->r;
					}
					else
						d=d->r;
					break;
				case 's':
				case 'S':					
					if(d->s==NULL)
					{
						d->s=newDict();
						d=d->s;
					}
					else
						d=d->s;
					break;
				case 't':
				case 'T':					
					if(d->t==NULL)
					{
						d->t=newDict();
						d=d->t;
					}
					else
						d=d->t;
					break;
				case 'u':
				case 'U':					
					if(d->u==NULL)
					{
						d->u=newDict();
						d=d->u;
					}
					else
						d=d->u;
					break;
				case 'v':
				case 'V':					
					if(d->v==NULL)
					{
						d->v=newDict();
						d=d->v;
					}
					else
						d=d->v;
					break;
				case 'w':
				case 'W':					
					if(d->w==NULL)
					{
						d->w=newDict();
						d=d->w;
					}
					else
						d=d->w;
					break;


				case 'x':
				case 'X':					
					if(d->x==NULL)
					{
						d->x=newDict();
						d=d->x;
					}
					else
						d=d->x;
					break;

				case 'y':
				case 'Y':					
					if(d->i==NULL)
					{
						d->y=newDict();
						d=d->y;
					}
					else
						d=d->y;
					break;

				case 'z':
				case 'Z':					
					if(d->z==NULL)
					{
						d->z=newDict();
						d=d->z;
					}
					else
						d=d->z;
					break;
			}
		
		}

		if(status==1 && d->valid==1)
		{
			printf("\n[%s] Repeat Enter Word.\n",word);
			return 0;
		}
		else
		{
			d->valid=1;
			strcpy(d->data,word);
			strcpy(d->def,def);
			return 1;
		}

	}

void searchDict(Dict *d,char *word)
	{
		//printf("[%d]",strlen(word));
		int flag=0;
		for(int i=0;i<strlen(word);i++)
		{
			switch(word[i])
			{
				case 'a':
					if(d->a!=NULL)
						d=d->a;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'b':
				case 'B':
					if(d->b!=NULL)
						d=d->b;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'c':
				case 'C':
					if(d->c!=NULL)
						d=d->c;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'd':
				case 'D':
					if(d->d!=NULL)
						d=d->d;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'e':
				case 'E':
					if(d->e!=NULL)
						d=d->e;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'f':
				case 'F':
					if(d->f!=NULL)
						d=d->f;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'g':
				case 'G':
					if(d->g!=NULL)
						d=d->g;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'h':
				case 'H':
					if(d->h!=NULL)
						d=d->h;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'i':
				case 'I':
					if(d->i!=NULL)
						d=d->i;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'j':
				case 'J':
					if(d->j!=NULL)
						d=d->j;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'k':
				case 'K':
					if(d->k!=NULL)
						d=d->k;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'l':
				case 'L':
					if(d->l!=NULL)
						d=d->l;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'm':
				case 'M':
					if(d->m!=NULL)
						d=d->m;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'n':
				case 'N':
					if(d->n!=NULL)
						d=d->n;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'o':
				case 'O':
					if(d->o!=NULL)
						d=d->o;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'p':
				case 'P':
					if(d->p!=NULL)
						d=d->p;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'q':
				case 'Q':
					if(d->q!=NULL)
						d=d->q;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'r':
				case 'R':
					if(d->r!=NULL)
						d=d->r;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 's':
				case 'S':
					if(d->s!=NULL)
						d=d->s;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 't':
				case 'T':
					if(d->t!=NULL)
						d=d->t;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'u':
				case 'U':
					if(d->u!=NULL)
						d=d->u;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'v':
				case 'V':
					if(d->v!=NULL)
						d=d->v;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'w':
				case 'W':
					if(d->w!=NULL)
						d=d->w;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;


				case 'x':
				case 'X':
					if(d->x!=NULL)
						d=d->x;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;

				case 'y':
				case 'Y':
					if(d->y!=NULL)
						d=d->y;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;


				case 'z':
				case 'Z':
					if(d->z!=NULL)
						d=d->z;
					else
					{
						printf("\n[%s] Word Not present\n",word);
						flag=1;
						break;
					}
					break;
				default:
						printf("\n%s Word Not valid",word);
						flag=1;
						break;

			}//end switch loop
				if(flag==1)
					break;
			}//end for loop
		
			if(d->valid==1 && flag==0)
			{
				printf("\n[%s] = [DEF] %s",word,d->def);
			}
			if(d->valid==-1 && flag==0)
				printf("\n[%s] Word Not Present\n",word);
	}

